#ifndef __Regex__Regex__
#define __Regex__Regex__

// C
#include <syslog.h>

// C++
#include <string>
#include <iostream>

// Boost
#include <boost/regex.hpp>
#include <boost/property_tree/ptree.hpp>

// Eloquent
#include "Eloquent/Extensions/Filters/Filter.h"
#include "Eloquent/Extensions/Filters/FilterFactory.h"

namespace Eloquent {
	///////////////////////////////////////////////////////////////////////////////
	// Regex : Filter
	///////////////////////////////////////////////////////////////////////////////
	class Regex : public Filter {
	public:
		Regex( const  boost::property_tree::ptree::value_type& i_Config )
		: Filter( i_Config )
		, m_Pattern( m_Config.second.get<std::string>( "pattern" ) )
		, m_Replace( m_Config.second.get_optional<std::string>( "replace" ) )
		{}
		
		virtual ~Regex() {}
		
		virtual bool operator<<( std::string& io_Data ) {
			bool Matched = boost::regex_match( io_Data, boost::regex( m_Pattern ) );
			
			if( Matched && m_Replace.is_initialized() ) {
				io_Data = boost::regex_replace( io_Data, boost::regex( m_Pattern ), m_Replace.get() );
			}
			
			return Continue( Matched );

		}
		
	private:
		std::string	m_Pattern;
		boost::optional<std::string> m_Replace;
		
	};
	
	///////////////////////////////////////////////////////////////////////////////
	// RegexFactory : FilterFactory
	///////////////////////////////////////////////////////////////////////////////
	class RegexFactory : public FilterFactory {
	public:
		RegexFactory() : FilterFactory() {}
		virtual ~RegexFactory() {}
		
		virtual Filter* New( const boost::property_tree::ptree::value_type& i_ConfigNode ) {
			return new Regex( i_ConfigNode );
		}
		
	};
	
}

extern "C" void* Attach(void);
extern "C" void* Attach(void) { return new Eloquent::RegexFactory(); }

#endif /* defined(__Regex__Regex__) */
